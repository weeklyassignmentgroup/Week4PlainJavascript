const express = require("express")
const app = express()
const path = require('path')
const router = express.Router()
const packageJson = require('./package.json')

const requestListener = function (req, res) {
  res.sendFile(path.join(__dirname+'/src/index.html'));
};

router.get('/', requestListener);

router.get('/index.css', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/index.css'));
});

router.get('/bank.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/bank.js'));
});

router.get('/work.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/work.js'));
});

router.get('/main.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/main.js'));
});

router.get('/Dummy/testPerson.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Dummy/testPerson.js'));
});

router.get('/Dummy/easteregg.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Dummy/easteregg.js'));
});

router.get('/Resources/website.png', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Resources/website.png'));
});

router.get('/Observable/person.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Observable/person.js'));
});
router.get('/Observers/InitObservers.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Observers/InitObservers.js'));
});
router.get('/Observers/PersonObservers.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/Observers/PersonObservers.js'));
});
router.get('/buyLaptop.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/buyLaptop.js'));
});

router.get('/laptop.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/laptop.js'));
});

router.get('/main.js', function (req, res) {
  res.sendFile(path.join(__dirname+'/src/main.js'));
});

app.use(requireHTTPS);
app.use('/', router)
app.listen(process.env.PORT || 8080, () => console.log('Server started...'));


/**
 * @author: Klement Omeri
 * Special thanks to Klement for providing the function to redirect traffic from http to https
 * I found this in lecture
 */
 function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if (!req.secure && req.get('x-forwarded-proto') !== 'https') {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}
