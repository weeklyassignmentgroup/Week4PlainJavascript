# Week4PlainJavascript

## Description
A simple webpage that allows you to work hard and gain bank balance. After working a while you can choose from a selection of laptops and buy any of them you want. 

## Website frontpage
![image.png](./website.png)

## Technologies used
Javascript
HTML
CSS

## Contributors
- [Øyvind Sande Reitan](https://gitlab.com/hindrance)
