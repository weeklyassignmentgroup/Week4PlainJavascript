export class Person {
  //_names to not conflict with setters and getters
  constructor(name, bankBalance, loanAmount, currency, salaryAmount){
    this.name = name;
    this._bankBalance = bankBalance;
    this._loanAmount = loanAmount;
    this.currency = currency;
    this._salaryAmount = salaryAmount;
    this.LoanObservers = [];
    this.SalaryObservers = [];
    this.BalanceObservers = [];
  }

  set salaryAmount(value){
    this._salaryAmount = value;
    this.notifyAllSalaryObservers();
  }

  get salaryAmount(){
    return this._salaryAmount;
  }

  set bankBalance(value){
    this._bankBalance = value;
    this.notifyAllBalanceObservers();
  }

  get bankBalance(){
    return this._bankBalance;
  }
   
  set loanAmount(value){
    if(value < 0) alert("You cannot have a negative loanAmount");
    this._loanAmount = value;
    this.notifyAllLoanObservers();
  }

  get loanAmount(){
    return this._loanAmount;
  }
  
  //Observers of different properties
  //Could be transformed into pub/sub pattern if you want even looser coupling
  subscribeLoan(observer){
    this.LoanObservers.push(observer);
    console.log(observer);
  }
  notifyAllLoanObservers(){
    this.LoanObservers.forEach((observer) => {
      observer.notify(this.loanAmount, this.currency);
    })
  }

  subscribeSalary(observer){
    this.SalaryObservers.push(observer);
    console.log(observer);
  }
  notifyAllSalaryObservers(){
    this.SalaryObservers.forEach((observer) => {
      observer.notify(this._salaryAmount, this.currency);
    })
  }

  subscribeBalance(observer){
    this.BalanceObservers.push(observer);
    console.log(observer);
  }
  notifyAllBalanceObservers(){
    this.BalanceObservers.forEach((observer) => {
      observer.notify(this._bankBalance, this.currency);
    })
  }

}

