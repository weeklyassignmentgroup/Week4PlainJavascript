export class Laptop {
  constructor(elLaptopImage, elLaptopTitle, elLaptopSpecs, elLaptopDesc,elLaptopPrice, elLaptopSelect ){
    this.elLaptopImage = elLaptopImage;
    this.elLaptopTitle = elLaptopTitle;
    this.elLaptopSpecs = elLaptopSpecs;
    this.elLaptopDesc = elLaptopDesc;
    this.elLaptopPrice= elLaptopPrice;
    this.elLaptopSelect = elLaptopSelect;
  }

  UpdatedLaptopSelection(getOne, currentLaptopPrice, currency) {
    const laptopId = this.elLaptopSelect.value;
    const imageHubBaseUrl = "https://noroff-komputer-store-api.herokuapp.com/";
  
    if (laptopId === ""){
      this.elLaptopImage.src = "Resources/website.png"
      currentLaptopPrice.value = 0;
      return;
    }
    const selectedLaptop = getOne(laptopId);
    this.elLaptopImage.src = "Resources/website.png"
    this.elLaptopImage.src = imageHubBaseUrl + selectedLaptop.image;
    //But the fetch is just for the data why would image render slower
    this.elLaptopImage.alt = selectedLaptop.title + " image";
    this.elLaptopTitle.innerText = `${selectedLaptop.title}`;
  
    this.elLaptopSpecs.innerHTML = "";
    for (const key in selectedLaptop.specs) {
      const spec = selectedLaptop.specs[key];
      this.elLaptopSpecs.innerHTML += `<li>${spec}</li>`;
    }
    this.elLaptopPrice.innerHTML = `${selectedLaptop.price} ${currency}`;
    this.elLaptopDesc.innerHTML = `<p>${selectedLaptop.description}</p>`
    console.log(currentLaptopPrice.value)
  
    currentLaptopPrice.value = selectedLaptop.price;
  }
  





}