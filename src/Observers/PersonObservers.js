//What should happen when loan changes
export function LoanObserver (domLoanLabel, domLoanAmount, domRepayBtn){
  return {
    domLoanLabel,
    domLoanAmount,
    domRepayBtn,
    notify: function(newLoanAmount, currency){
      if(newLoanAmount === 0) {
        domLoanLabel.style.display  = "none";
        domRepayBtn.style.display = "none";
      }
      if(newLoanAmount  > 0) {
        domLoanLabel.style.display = "grid";
        domRepayBtn.style.display = "grid";
        domLoanAmount.innerHTML=`${newLoanAmount}${currency}`;
      }
    }
  }
}

//What should happen when salaryamount changes
export function SalaryObserver (domSalaryAmount){
  return {
    domSalaryAmount,
    notify: function(newSalaryAmount, currency){
      domSalaryAmount.innerHTML=`${newSalaryAmount}${currency}`;
      
    }
  }
}

//What should happen when BankBalance changes
export function BankBalanceObserver (domBalanceAmount){
  return {
    domBalanceAmount,
    notify: function(newBankBalanceAmount, currency){
      domBalanceAmount.innerHTML=`${newBankBalanceAmount}${currency}`;
    }
  }
}
