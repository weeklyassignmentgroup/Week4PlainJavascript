
//Attempt to take a loan
export function takeLoan(person) {
  const newLoanAmount = prompt(
    "Enter amount of your currency you wish to loan?"
  );

  if (isNaN(newLoanAmount)) {
    alert("You need to input only the number");
    return;
  }

  var back = checkEnoughBankBalance(person, newLoanAmount);
  if (back != undefined) {
    person.loanAmount = back;
    person.bankBalance = +person.bankBalance + +person.loanAmount;
  }
}

//Checks if you have enough bank balance to take the loan
function checkEnoughBankBalance(person, newLoanAmount) {
  return (newLoanAmount <= person.bankBalance * 2)
    ? checkOneLoan(person, newLoanAmount)
    : alert("You can only make a loan double your bank balance");
}

//Checks if you allready have a loan
function checkOneLoan(person, newLoanAmount) {
  return (person.loanAmount <= 0)
    ? newLoanAmount
    : alert("Sorry, you are only allowed one loan at a time");
}
