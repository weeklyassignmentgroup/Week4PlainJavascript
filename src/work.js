
export function addWorkPay(TestPerson){
  const workPayment = 100;
  TestPerson.salaryAmount += workPayment;
}

export function depositSalary(TestPerson){
  if(TestPerson.loanAmount > 0){
    var loanPayment = calculateLoanPayment(TestPerson.salaryAmount, TestPerson.loanAmount, 0.1);
    TestPerson.bankBalance += TestPerson.salaryAmount - loanPayment;
    TestPerson.loanAmount -= loanPayment;
  }else{
    TestPerson.bankBalance += TestPerson.salaryAmount;
  } 

  TestPerson.salaryAmount = 0;
}

export function calculateLoanPayment(salaryAmount, loanAmount, percentage){
  var loanPay = salaryAmount*percentage;
  return (loanAmount >= loanPay) ? loanPay : loanAmount;
}

export function repayLoan(TestPerson){
  var loanPayment = calculateLoanPayment(TestPerson.salaryAmount, TestPerson.loanAmount, 1.0);
  TestPerson.bankBalance += TestPerson.salaryAmount - loanPayment;
  TestPerson.loanAmount -= loanPayment;
  TestPerson.salaryAmount = 0;
}

