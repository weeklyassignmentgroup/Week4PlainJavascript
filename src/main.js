import { SalaryObserver, LoanObserver, BankBalanceObserver } from "./Observers/PersonObservers.js";
import { TestPerson } from "./Dummy/testPerson.js";
import { addWorkPay, depositSalary, repayLoan } from "./work.js";
import { takeLoan } from "./bank.js";
import { BuyLaptop } from "./buyLaptop.js";
import { Laptop } from "./laptop.js";
//Main file for initial setup


//Work
const elBankBtn = document.getElementById("bankBtn");
const elWorkBtn = document.getElementById("workBtn");
const elRepayLoanBtn = document.getElementById("repayLoanBtn");
elWorkBtn.addEventListener('click', function(){ addWorkPay(TestPerson); });
elBankBtn.addEventListener('click', function(){ depositSalary(TestPerson); });
elRepayLoanBtn.addEventListener('click', function(){ repayLoan(TestPerson); });

//Bank
const elLoan = document.getElementById("getLoanBtn");
elLoan.addEventListener("click", function () {
  takeLoan(TestPerson);
});

//Observers setup
//Observers will update DOM to be in the same state as my TestPerson
const elSalaryAmount = document.getElementById("salaryAmount");
const elBalanceAmount = document.getElementById("balanceAmount");
const elLoanLabel = document.getElementById("loan");
const elLoanAmountLabel = document.getElementById("loanAmount");

const SalaryObs = new SalaryObserver(elSalaryAmount);
TestPerson.subscribeSalary(SalaryObs);

const BankObs = new BankBalanceObserver(elBalanceAmount);
TestPerson.subscribeBalance(BankObs);

const LoanObs = new LoanObserver(
  elLoanLabel,
  elLoanAmountLabel,
  elRepayLoanBtn
);
TestPerson.subscribeLoan(LoanObs);



//Init values in the dom
elBalanceAmount.innerHTML = TestPerson.bankBalance + TestPerson.currency;
elSalaryAmount.innerHTML = TestPerson.salaryAmount + TestPerson.currency;




const elLaptopBuyNow = document.getElementById("buyNow");
const elLaptopSelect = document.getElementById("laptop");
const elLaptopImage = document.getElementById("pcImg");
const elLaptopTitle = document.getElementById("pcName");
const elLaptopSpecs = document.getElementById("description");
const elLaptopDesc = document.getElementById("pcDescription");
const elLaptopPrice = document.getElementById("pcPrice");
//Initializing a laptop object with the relevant dom elements
var elLaptop = new Laptop(elLaptopImage, elLaptopTitle, elLaptopSpecs, elLaptopDesc, elLaptopPrice, elLaptopSelect);

//Fetching initial list of laptops
async function fetchLaptop() {
  try {
    const response = await fetch(
      "https://noroff-komputer-store-api.herokuapp.com/computers"
    );
    return response.json();
  } catch {
    console.error("Failed to fetch data");
  }
}


const getOneLaptop = await (async () => {
  const allLaptopsJson = await fetchLaptop();
  for (const laptop of allLaptopsJson) {
    elLaptopSelect.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`;
  }
  // Closure
  return function(laptopId){ return allLaptopsJson.find((lap) => lap.id == laptopId)};
})(); //IFef

//pass value by object
var currentLaptopPrice = {value:0}; 
elLaptopSelect.addEventListener("change", function(){elLaptop.UpdatedLaptopSelection(getOneLaptop, currentLaptopPrice, TestPerson.currency)});
elLaptopBuyNow.addEventListener("click", function(){BuyLaptop(currentLaptopPrice)})
















